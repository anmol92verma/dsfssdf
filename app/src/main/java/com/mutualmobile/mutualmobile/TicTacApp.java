package com.mutualmobile.mutualmobile;

import android.app.Application;
import android.content.Intent;

import com.google.firebase.database.FirebaseDatabase;
import com.mutualmobile.mutualmobile.service.ListenGames;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by Anmol on 9/30/16.
 */

public class TicTacApp extends Application {

    public static Bus busProvider;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        startService(new Intent(new Intent(this, ListenGames.class)));

        busProvider = new Bus(ThreadEnforcer.MAIN);
        busProvider.register(this);
    }

    public static Bus getBusProvider() {
        return busProvider;
    }
}
