package com.mutualmobile.mutualmobile.persistance;


import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Anmol on 9/30/16.
 */

public class TicTacPreferences {

    public static TicTacPreferences instance;
    private final String SHAREDPREF_NAME = "SHAREDPREF_NAME";
    private final SharedPreferences sharedPreferences;
    private String USER_O_POINTS = "USER_O_POINTS";
    private String USER_X_POINTS = "USER_X_POINTS";
    private String USER_EMAIL = "USER_EMAIL";

    public TicTacPreferences(Context applicationContext) {
        sharedPreferences = applicationContext.getSharedPreferences(SHAREDPREF_NAME, Context.MODE_PRIVATE);
    }

    public static TicTacPreferences getInstance(Context applicationContext) {
        Object object = new Object();
        synchronized (object) {
            if (instance == null) {
                instance = new TicTacPreferences(applicationContext);
            } else {
                return instance;
            }
        }
        return instance;
    }

    public void saveUserData(int user_points_user_o, int user_points_user_x) {
        sharedPreferences.edit().putInt(USER_O_POINTS, user_points_user_o).apply();
        sharedPreferences.edit().putInt(USER_X_POINTS, user_points_user_x).apply();
    }

    public int getXScore() {
        return sharedPreferences.getInt(USER_X_POINTS, 0);
    }

    public int getOScore() {
        return sharedPreferences.getInt(USER_O_POINTS, 0);
    }

    public void setMyName(String myName) {
        sharedPreferences.edit().putString(USER_EMAIL, myName).apply();
    }

    public String getMyName() {
       return sharedPreferences.getString(USER_EMAIL, "");
    }
}
