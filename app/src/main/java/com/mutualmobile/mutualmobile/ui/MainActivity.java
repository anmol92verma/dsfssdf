package com.mutualmobile.mutualmobile.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mutualmobile.mutualmobile.R;
import com.mutualmobile.mutualmobile.TicTacApp;
import com.mutualmobile.mutualmobile.models.Game;
import com.mutualmobile.mutualmobile.models.GameMove;
import com.mutualmobile.mutualmobile.models.Move;
import com.mutualmobile.mutualmobile.models.User;
import com.mutualmobile.mutualmobile.models.UserScores;
import com.mutualmobile.mutualmobile.persistance.FireBaseTicTac;
import com.mutualmobile.mutualmobile.persistance.TicTacPreferences;
import com.mutualmobile.mutualmobile.service.InitiatedGame;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.squareup.otto.ThreadEnforcer;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    public static String GAME = "game";
    private Unbinder binder;

    @BindView(R.id.tvScoreOne)
    TextView tvScoreOne;
    @BindView(R.id.tvTurnUser)
    TextView tvTurnUser;
    @BindView(R.id.tvScoreTwo)
    TextView tvScoreTwo;
    private int[][] mMatrix;

    @BindView(R.id.tableRowOne)
    TableRow tableRowone;

    @BindView(R.id.tableRowTwo)
    TableRow tableRowTwo;

    @BindView(R.id.tableRowThree)
    TableRow tableRowThree;

    private int mWhoseMove;
    private final int USER_X = 1;
    private final int USER_O = 0;

    private int USER_POINTS_USER_X = 0;
    private int USER_POINTS_USER_O = 0;


    private String TAG = MainActivity.class.getName();
    private String email;
    private ProgressDialog mProgress;
    private FirebaseDatabase database;
    private final int TYPE_ENTER_OTHER_USER = 2;
    private Game gameData;
    private final int TYPE_ENTER_YOURSELF = 1;
    private String turnUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TicTacApp.getBusProvider().register(this);
        setContentView(R.layout.activity_main);
        binder = ButterKnife.bind(this);

        if (!handleIntent(getIntent())) {
            askUserForCredentials();
        }
    }


    @Subscribe
    public void initiatedGame(final InitiatedGame initiatedGame) {
        if (!isFinishing()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    gameData = initiatedGame.getGame();
                    if (initiatedGame.getGame().initiated) {
                        initializeMyMatrix();
                        showToast(getString(R.string.string_otheruser_joined));
                    }
                }
            });
        }

    }


    @Subscribe
    public void moveMove(Move move) {
        if (!isFinishing()) {

            mMatrix[move.value.matrixY][move.value.matrixY] = move.value.value;
            Button thebutton = (Button) findViewById(move.value.buttonid);


            mWhoseMove = move.value.value;


            if (mWhoseMove == USER_X) {
                thebutton.setText("X");
            } else {
                thebutton.setText("" + USER_O);
            }

            boolean userWon = checkIfUserWon();
            if (userWon) {
                initializeMyMatrix();
            } else {
                if (mWhoseMove == USER_X) {
                    mWhoseMove = USER_O;
                    showToast(getString(R.string.string_turn_zero));

                    turnUser = gameData.userOne;
                    tvTurnUser.setText("The turn is for " + turnUser);
                } else {
                    mWhoseMove = USER_X;
                    showToast(getString(R.string.string_turn_ex));

                    turnUser = gameData.userTwo;
                    tvTurnUser.setText("The turn is for " + turnUser);

                }
            }
        }
    }

    private Button findButtonForMove(Move move) {
        return null;
    }

    private boolean handleIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null && intent.getParcelableExtra(GAME) != null) {
            gameData = intent.getParcelableExtra(GAME);
            initiateGameForBothPeople(gameData);
            return true;
        } else {
            return false;
        }
    }

    private void initiateGameForBothPeople(Game game) {
        showDialogProgress();
        database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(FireBaseTicTac.GAMES);
        game.initiated = true;
        myRef.setValue(game, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                hideDialog();
                initializeMyMatrix();
            }
        });
    }

    private void askUserForCredentials() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(getDialogView(TYPE_ENTER_YOURSELF));
        builder.setCancelable(false);
        builder.setNegativeButton(R.string.string_exit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).setPositiveButton(R.string.string_cool, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                email = getUserEmail();
                TicTacPreferences.getInstance(getApplicationContext()).setMyName(email);
                if (!TextUtils.isEmpty(email)) {
                    if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                        String name = email.split("@")[0];
                        writeNewUser(name, email);
                    } else {
                        showToast(getString(R.string.string_enter_email_address));
                        askUserForCredentials();
                    }
                } else {
                    showToast(getString(R.string.string_enter_email_address));
                }
            }
        });
        AlertDialog mDialog = builder.create();
        mDialog.show();
    }

    private void writeNewUser(String name, final String email) {
        showDialogProgress();
        User user = new User(name, email);
        database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(FireBaseTicTac.TABLE_USERS);

        Map<String, Object> postValues = user.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/" + FireBaseTicTac.TABLE_USER + " / " + getEmailWithat(email), postValues);

        myRef.updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                hideDialog();
                nowInitiateGame(email);
            }
        });
    }

    private String getEmailWithat(String email) {
        String withoutat = email.replaceAll("@", "");
        withoutat = withoutat.replaceAll("\\.", "");
        return withoutat;
    }


    private void nowInitiateGame(final String emailAddress) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(getDialogView(TYPE_ENTER_OTHER_USER));
        builder.setCancelable(false);
        builder.setNegativeButton(R.string.string_exit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).setPositiveButton(R.string.string_cool, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String otherEmail = getUserEmail();
                if (!TextUtils.isEmpty(otherEmail)) {
                    if (Patterns.EMAIL_ADDRESS.matcher(otherEmail).matches()) {
//                        String name = otherEmail.split("@")[0];
                        saveGameToDb(otherEmail, emailAddress);
                    } else {
                        showToast(getString(R.string.string_enter_email_address));
                        nowInitiateGame(emailAddress);
                    }
                } else {
                    showToast(getString(R.string.string_enter_email_address));
                }


            }
        });
        AlertDialog mDialog = builder.create();
        mDialog.show();


    }

    private void saveGameToDb(String otherUser, String primaryEmail) {
        showDialogProgress();
        DatabaseReference myRef = database.getReference(FireBaseTicTac.GAMES);
        Game game = new Game();
        game.gameId = System.currentTimeMillis();
        game.userOne = primaryEmail;
        game.userTwo = otherUser;
        game.initiated = false;
        myRef.setValue(game, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                hideDialog();
                showToast("Game Request Sent to User!");
            }
        });
    }

    private void showDialogProgress() {
        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Wait!!!!");
        mProgress.show();
    }

    private void hideDialog() {
        mProgress.hide();
    }


    @Override
    protected void onResume() {
        super.onResume();
        reloadMyPoints();
    }

    private void reloadMyPoints() {
        USER_POINTS_USER_X = TicTacPreferences.getInstance(getApplicationContext()).getXScore();
        USER_POINTS_USER_O = TicTacPreferences.getInstance(getApplicationContext()).getOScore();

        tvScoreOne.setText(String.valueOf(USER_POINTS_USER_O));
        tvScoreTwo.setText(String.valueOf(USER_POINTS_USER_X));
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveMyPoints();
        saveScoresToFirebase();
    }

    private void saveMyPoints() {
        TicTacPreferences.getInstance(getApplicationContext()).saveUserData(USER_POINTS_USER_O, USER_POINTS_USER_X);
    }

    private void saveScoresToFirebase() {
        UserScores user = new UserScores();
        user.setScoreX(USER_POINTS_USER_X);
        user.setScoreY(USER_POINTS_USER_O);
    }

    private void initializeMyMatrix() {
        mMatrix = new int[3][3];
        mWhoseMove = USER_O;
        turnUser = gameData.userOne;
        tvTurnUser.setText("The turn is for " + turnUser);
        // Initialize with -1 for nothing!
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                mMatrix[i][j] = -1;
            }
        }

        for (int i = 0; i < tableRowone.getChildCount(); i++) {
            ((Button) tableRowone.getChildAt(i)).setText("");
        }

        for (int i = 0; i < tableRowTwo.getChildCount(); i++) {
            ((Button) tableRowTwo.getChildAt(i)).setText("");
        }

        for (int i = 0; i < tableRowThree.getChildCount(); i++) {
            ((Button) tableRowThree.getChildAt(i)).setText("");
        }

    }


    @OnClick({R.id.btnOne,
            R.id.btnTwo, R.id.btnthree,
            R.id.btnFour, R.id.btnFive, R.id.btnSeven,
            R.id.btnSix, R.id.btnEight, R.id.btnNine})
    public void onClick(View view) {

        if (mMatrix == null) {
            showToast("Game not Ready!");
            return;
        }
        int matrixX = 0, matrixY = 0;

        switch (view.getId()) {
            case R.id.btnOne:
                matrixX = 0;
                matrixY = 0;
                break;
            case R.id.btnTwo:
                matrixX = 0;
                matrixY = 1;
                break;
            case R.id.btnthree:
                matrixX = 0;
                matrixY = 2;
                break;
            case R.id.btnFour:
                matrixX = 1;
                matrixY = 0;
                break;
            case R.id.btnFive:
                matrixX = 1;
                matrixY = 1;
                break;
            case R.id.btnSix:
                matrixX = 1;
                matrixY = 2;
                break;
            case R.id.btnSeven:
                matrixX = 2;
                matrixY = 0;
                break;
            case R.id.btnEight:
                matrixX = 2;
                matrixY = 1;
                break;
            case R.id.btnNine:
                matrixX = 2;
                matrixY = 2;
                break;

        }
        presentIt(matrixX, matrixY, view);

    }

    private void presentIt(int matrixX, int matrixY, View view) {
        if (mMatrix[matrixX][matrixY] == -1) {
            // Proceed
            Button thebutton = null;
            if (view instanceof Button) {
                thebutton = (Button) view;
            }
            if (mWhoseMove == USER_X) {
                // mMatrix[matrixX][matrixY] = USER_X;
                if (thebutton != null)
                    thebutton.setText("X");
            } else {
                // mMatrix[matrixX][matrixY] = USER_O;
                if (thebutton != null)
                    thebutton.setText("" + USER_O);
            }

            sharePressedMatrixData(matrixX, matrixY, mMatrix[matrixX][matrixY], view.getId());
            boolean userWon = checkIfUserWon();
            if (userWon) {
                initializeMyMatrix();
            } else {
                if (mWhoseMove == USER_X) {
                    mWhoseMove = USER_O;
                    showToast(getString(R.string.string_turn_zero));

                    turnUser = gameData.userOne;
                    tvTurnUser.setText("The turn is for " + turnUser);
                } else {
                    mWhoseMove = USER_X;
                    showToast(getString(R.string.string_turn_ex));

                    turnUser = gameData.userTwo;
                    tvTurnUser.setText("The turn is for " + turnUser);

                }
            }
        }
    }

    private void sharePressedMatrixData(int matrixX, int matrixY, int value, int id) {

        showDialogProgress();
        DatabaseReference myRef = database.getReference(FireBaseTicTac.TABLE_MOVES);

        GameMove gameMove = new GameMove(value, matrixX, matrixY, turnUser, id);

        myRef.setValue(gameMove, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                hideDialog();
            }
        });


    }

    private boolean checkIfUserWon() {
        // Horizontal Lines

        boolean userWon = false;

        for (int i = 0; i != 3; ++i) {
            if (mMatrix[i][0] == USER_X &&
                    mMatrix[i][1] == USER_X &&
                    mMatrix[i][2] == USER_X) {
                String mResult = USER_X + " wins!";
                showToast(mResult);
                saveScore(USER_X);

                userWon = true;
                Log.e(TAG, mResult);
            }

            if (mMatrix[i][0] == USER_O &&
                    mMatrix[i][1] == USER_O &&
                    mMatrix[i][2] == USER_O) {
                String mResult = USER_O + " wins!";
                showToast(mResult);
                saveScore(USER_O);

                userWon = true;
                Log.e(TAG, mResult);
            }
        }

        // Vertical Lines

        for (int i = 0; i != 3; ++i) {
            if (mMatrix[0][i] == USER_X &&
                    mMatrix[1][i] == USER_X &&
                    mMatrix[2][i] == USER_X) {
                String mResult = USER_X + " wins!";
                showToast(mResult);
                saveScore(USER_X);

                userWon = true;
                Log.e(TAG, mResult);
            }

            if (mMatrix[0][i] == USER_O &&
                    mMatrix[1][i] == USER_O &&
                    mMatrix[2][i] == USER_O) {
                String mResult = USER_O + " wins!";
                showToast(mResult);
                saveScore(USER_O);

                userWon = true;
                Log.e(TAG, mResult);
            }
        }


        // Diagonal Lines

        if (mMatrix[0][0] == USER_X && mMatrix[1][1] == USER_X && mMatrix[2][2] == USER_X) {
            String mResult = USER_X + " wins!";
            showToast(mResult);
            saveScore(USER_X);

            userWon = true;
            Log.e(TAG, mResult);
        }

        if (mMatrix[0][0] == USER_O && mMatrix[1][1] == USER_O && mMatrix[2][2] == USER_O) {
            String mResult = USER_O + " wins!";
            showToast(mResult);
            saveScore(USER_O);

            userWon = true;
            Log.e(TAG, mResult);
        }

        if (mMatrix[0][2] == USER_X && mMatrix[1][1] == USER_X && mMatrix[2][0] == USER_X) {
            String mResult = USER_X + " wins!";
            showToast(mResult);
            saveScore(USER_X);

            userWon = true;
            Log.e(TAG, mResult);
        }

        if (mMatrix[0][2] == USER_O && mMatrix[1][1] == USER_O && mMatrix[2][0] == USER_O) {
            String mResult = USER_O + " wins!";
            showToast(mResult);
            saveScore(USER_O);
            userWon = true;
            Log.e(TAG, mResult);
        }


        return userWon;

    }

    private void saveScore(int user_o) {
        switch (user_o) {
            case USER_O:
                USER_POINTS_USER_O++;
                tvScoreOne.setText(String.valueOf(USER_POINTS_USER_O));
                break;
            case USER_X:
                USER_POINTS_USER_X++;
                tvScoreTwo.setText(String.valueOf(USER_POINTS_USER_X));
                break;
        }
    }

    private void showToast(String mResult) {
        Snackbar.make(tvScoreOne, mResult, Snackbar.LENGTH_SHORT).show();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        binder.unbind();
        TicTacApp.getBusProvider().unregister(this);
    }

    EditText edtDialogEmail;

    public View getDialogView(int typeUser) {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.layout_email_dialog, null);
        switch (typeUser) {
            case TYPE_ENTER_OTHER_USER:
                TextView viewOne = (TextView) dialogView.findViewById(R.id.viewOne);
                viewOne.setText("Please enter email of other user");
                break;
            case TYPE_ENTER_YOURSELF:
                break;
        }
        edtDialogEmail = (EditText) dialogView.findViewById(R.id.edtDialogEmail);
        return dialogView;
    }

    public String getUserEmail() {
        return edtDialogEmail.getText().toString();
    }
}
