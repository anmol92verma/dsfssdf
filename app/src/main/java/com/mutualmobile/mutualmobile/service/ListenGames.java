package com.mutualmobile.mutualmobile.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mutualmobile.mutualmobile.R;
import com.mutualmobile.mutualmobile.TicTacApp;
import com.mutualmobile.mutualmobile.models.Game;
import com.mutualmobile.mutualmobile.models.GameMove;
import com.mutualmobile.mutualmobile.models.Move;
import com.mutualmobile.mutualmobile.persistance.FireBaseTicTac;
import com.mutualmobile.mutualmobile.persistance.TicTacPreferences;
import com.mutualmobile.mutualmobile.ui.MainActivity;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import java.util.HashMap;

import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by Anmol on 10/3/16.
 */

public class ListenGames extends Service {
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private DatabaseReference userMovesRef;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        TicTacApp.getBusProvider().register(this);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference(FireBaseTicTac.GAMES);
        userMovesRef = database.getReference(FireBaseTicTac.TABLE_MOVES);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TicTacApp.getBusProvider().unregister(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        userMovesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                if (dataSnapshot != null) {
                    GameMove value = dataSnapshot.getValue(GameMove.class);
                    if (value != null) {
                        TicTacApp.getBusProvider().post(new Move(value));
                    }
                }
            }


            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                if (dataSnapshot != null) {
                    Game value = dataSnapshot.getValue(Game.class);
                    if (value != null) {
                        if (value.userTwo.
                                equals(TicTacPreferences.getInstance(getApplicationContext()).getMyName())) {
                            Log.d(TAG, "Value is: " + value);
                            if (value.initiated) {
                                TicTacApp.getBusProvider().post(new InitiatedGame(value));
                            } else {
                                showNotification(value);
                            }
                        } else if (value.userOne.equals(TicTacPreferences.getInstance(getApplicationContext()).getMyName())) {
                            if (value.initiated) {
                                TicTacApp.getBusProvider().post(new InitiatedGame(value));
                            }
                        }
                    }
                }
            }


            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });


        return super.onStartCommand(intent, flags, startId);
    }

    private void showNotification(Game value) {
        // Set Notification Title
        String strtitle = getString(R.string.app_name);
        // Set Notification Text
        String strtext = getString(R.string.string_new_game);

        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(this, MainActivity.class);
        // Send data to NotificationView Class
        intent.putExtra(MainActivity.GAME, value);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //Create Notification using NotificationCompat.Builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                // Set Icon
                .setSmallIcon(R.mipmap.ic_launcher)
                // Set Ticker Message
                .setTicker(getString(R.string.string_new_game))
                // Set Title
                .setContentTitle(strtitle)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                // Set Text
                .setContentText(strtext + " from " + value.userOne)
                // Add an Action Button below Notification
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Dismiss Notification
                .setAutoCancel(true);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(0, builder.build());

    }
}
