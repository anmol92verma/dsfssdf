package com.mutualmobile.mutualmobile.service;

import com.mutualmobile.mutualmobile.models.Game;

/**
 * Created by Anmol on 10/3/16.
 */
public class InitiatedGame {
    public Game initiated;

    public InitiatedGame(Game initiated) {
        this.initiated = initiated;
    }

    public Game getGame() {
        return initiated;
    }
}
