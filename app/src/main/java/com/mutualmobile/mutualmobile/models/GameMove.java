package com.mutualmobile.mutualmobile.models;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anmol on 10/3/16.
 */
public class GameMove {
    public int value;
    public int matrixX;
    public int matrixY;
    public String moveUser;
    public int buttonid;

    public GameMove() {

    }

    public GameMove(int value, int matrixX, int matrixY, String moveUser, int id) {
        this.value = value;
        this.matrixX = matrixX;
        this.matrixY = matrixY;
        this.moveUser = moveUser;
        this.buttonid = id;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("value", value);
        result.put("matrixY", matrixY);
        result.put("matrixX", matrixX);
        result.put("moveUser", moveUser);
        result.put("buttonid", buttonid);
        return result;
    }
}
