package com.mutualmobile.mutualmobile.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by Anmol on 10/3/16.
 */

public class Game implements Parcelable {
    public long gameId;
    public String userOne;
    public String userTwo;
    public boolean initiated = false;


    @Override
    public String toString() {
        return "game id " + gameId + " userone" + userOne + " userTwo" + userTwo;
    }

    public Game() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.gameId);
        dest.writeString(this.userOne);
        dest.writeString(this.userTwo);
        dest.writeByte(this.initiated ? (byte) 1 : (byte) 0);
    }

    protected Game(Parcel in) {
        this.gameId = in.readLong();
        this.userOne = in.readString();
        this.userTwo = in.readString();
        this.initiated = in.readByte() != 0;
    }

    public static final Creator<Game> CREATOR = new Creator<Game>() {
        @Override
        public Game createFromParcel(Parcel source) {
            return new Game(source);
        }

        @Override
        public Game[] newArray(int size) {
            return new Game[size];
        }
    };
}
