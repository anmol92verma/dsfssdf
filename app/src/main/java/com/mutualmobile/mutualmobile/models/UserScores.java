package com.mutualmobile.mutualmobile.models;

/**
 * Created by Anmol on 9/30/16.
 */

public class UserScores {

    private int scoreX;
    private int scoreY;

    public int getScoreX() {
        return scoreX;
    }

    public int getScoreY() {
        return scoreY;
    }

    public void setScoreX(int scoreX) {
        this.scoreX = scoreX;

    }

    public void setScoreY(int scoreY) {
        this.scoreY = scoreY;
    }
}
